/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writeread;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Clint Shepherd
 */
public class Write {
    private String friend = " ";
    ArrayList alist = new ArrayList();
    public String name = JOptionPane.showInputDialog("name of file");
    public File fn = new File(name + ".txt");

    public Write(){
    }
    
    public void getInfo(){

        while (!friend.isEmpty()) {
            friend = JOptionPane.showInputDialog("Name a friend, please.");
            if (!friend.isEmpty()) {
                alist.add(friend);
            }
        }

    }
    
    public void writeToFile() throws IOException{
        try{
            FileWriter fw = new FileWriter(fn);
            Writer output = new BufferedWriter(fw);
            int sz = alist.size();
            for (int i=0;i<sz;i++){
                output.write(alist.get(i).toString() + "\n");
            }
            output.close();
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,"I cannot create that file.");
        }
    }
}
