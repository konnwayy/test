/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writeread;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Clint Shepherd
 */
public class Read {
    String fn = JOptionPane.showInputDialog("enter file name to print");
    
    String line;
    ArrayList alist = new ArrayList();
    
    public Read() throws FileNotFoundException, IOException{
        fn = fn +".txt";
        try{
            BufferedReader input = new BufferedReader(new FileReader(fn));
            if (!input.ready()){
                throw new IOException();
            }
            while((line = input.readLine()) != null){
                alist.add(line);
            }
            input.close();
        }
        catch(IOException e){
            System.out.print(e);
        }
        int sz = alist.size();
        for(int i = 0;i<sz;i++){
            System.out.println(alist.get(i).toString());
        }
    }
}
